GoAccess [![C build](https://github.com/allinurl/goaccess/actions/workflows/build-test.yml/badge.svg)](https://github.com/allinurl/goaccess/actions/workflows/build-test.yml) [![GoAccess](https://goaccess.io/badge)](https://goaccess.io)
========
## 这是什么？##
GoAccess 是一个开源的 **实时网页日志分析器** 和交互式查看器，它在 **终端** 上的 &ast;nix 系统或通过你的 **浏览器** 运行。它为系统管理员提供了 **快速** 且有价值的 HTTP 统计信息，这些管理员需要即时的可视化服务器报告。
更多信息请访问：[https://goaccess.io](https://goaccess.io/?src=gh)。

## 特性 ##
GoAccess 解析指定的网络日志文件，并将数据输出到 X 终端。特性包括：

* **完全实时**<br>
  所有面板和指标都定时在终端输出上每 200 毫秒更新一次，HTML 输出上每秒更新一次。

* **最少的配置需求**<br>
  你可以直接对它运行你的访问日志文件，选择日志格式，让 GoAccess 解析访问日志并显示统计信息。

* **跟踪应用响应时间**<br>
  跟踪服务请求所需的时间。如果你想要跟踪拖慢你网站速度的页面，这非常有用。

* **几乎所有的网络日志格式**<br>
  GoAccess 允许任何自定义日志格式字符串。预定义选项包括，Apache, Nginx, Amazon S3, Elastic Load Balancing, CloudFront 等。

* **增量日志处理**<br>
  需要数据持久化？GoAccess 有能力通过磁盘持久化选项增量处理日志。

* **只有一个依赖**<br>
  GoAccess 用 C 语言编写。运行它，你只需要 ncurses 作为依赖。就这些。它甚至拥有自己的 WebSocket 服务器 — http://gwsocket.io/。

* **访客**<br>
  通过小时或日期确定点击次数、访客、带宽和最慢请求的指标。

* **每个虚拟主机的指标**<br>
  有多个虚拟主机（服务器块）？它具有显示哪个虚拟主机消耗了大部分网络服务器资源的面板。

* **ASN（自治系统编号映射）**<br>
  非常适合检测恶意流量模式并相应地阻止它们。

* **可定制的颜色方案**<br>
  通过终端或简单地应用 HTML 输出上的样式表，使 GoAccess 适合你自己的颜色偏好/方案。

* **支持大型数据集**<br>
  由于其优化的内存哈希表，GoAccess 具有解析大型日志的能力。它具有非常好的内存使用情况和相当好的表现。此存储还支持磁盘持久化。

* **Docker 支持**<br>
  能够从上游构建 GoAccess 的 Docker 镜像。你仍然可以完全配置它，使用卷映射和编辑 `goaccess.conf`。请参阅下面的 [Docker](https://github.com/allinurl/goaccess#docker) 部分。

### 几乎所有的网络日志格式... ###
GoAccess 允许任何自定义日志格式字符串。预定义选项包括但不限于：

* Amazon CloudFront（下载分发）。
* Amazon Simple Storage Service (S3)
* AWS Elastic Load Balancing
* Combined Log Format (XLF/ELF) Apache | Nginx
* Common Log Format (CLF) Apache
* Google Cloud Storage.
* Apache 虚拟主机
* Squid 原生格式。
* W3C 格式 (IIS)。
* Caddy 的 JSON 结构化格式。
* Traefik 的 CLF 风味

## 为什么选择 GoAccess？##
GoAccess 被设计为一个快速的、基于终端的日志分析器。其核心思想是快速分析和实时查看网络服务器统计信息，而无需使用浏览器（如果你想要通过 SSH 快速分析你的访问日志，或者你只是喜欢在终端中工作，这很棒）。

虽然终端输出是默认输出，但它有能力生成一个完整的、自包含的实时 [**`HTML`**](https://rt.goaccess.io/?src=gh) 报告，以及一个 [**`JSON`**](https://goaccess.io/json?src=gh) 和 [**`CSV`**](https://goaccess.io/goaccess_csv_report.csv?src=gh) 报告。

你可以将其视为一个监控命令工具。

## 安装 ##
### 从发布版构建 ###
GoAccess 可以在 *nix 系统上编译和使用。

下载、解压并编译 GoAccess：

    $ wget https://tar.goaccess.io/goaccess-1.9.2.tar.gz
    $ tar -xzvf goaccess-1.9.2.tar.gz
    $ cd goaccess-1.9.2/
    $ ./configure --enable-utf8 --enable-geoip=mmdb
    $ make
    # make install

### 从 GitHub (开发版) 构建 ###
    $ git clone https://github.com/allinurl/goaccess.git
    $ cd goaccess
    $ autoreconf -fiv
    $ ./configure --enable-utf8 --enable-geoip=mmdb
    $ make
    # make install

#### 在隔离容器中构建 ####

你也可以在隔离的容器环境中为基于 Debian 的系统构建二进制文件，以防止在本地系统上用开发库弄乱：

    $ curl -L "https://github.com/allinurl/goaccess/archive/refs/heads/master.tar.gz" | tar -xz && cd goaccess-master
    $ docker build -t goaccess/build.debian-12 -f Dockerfile.debian-12 .
    $ docker run -i --rm -v $PWD:/goaccess goaccess/build.debian-12 > goaccess

### 版本发行版 ###
使用你 GNU/Linux 发行版的推荐包管理器在 GNU/Linux 上安装 GoAccess 最简单。请注意，并非所有发行版都有 GoAccess 的最新版本。

#### Debian/Ubuntu ####
    # apt-get install goaccess

**注意：** 这可能会安装 GoAccess 的过时版本。为了确保你运行的是 GoAccess 的最新稳定版本，请参阅下面的替代选项。

#### 官方 GoAccess Debian & Ubuntu 仓库 ####

    $ wget -O - https://deb.goaccess.io/gnugpg.key | gpg --dearmor \
        | sudo tee /usr/share/keyrings/goaccess.gpg >/dev/null
    $ echo "deb [signed-by=/usr/share/keyrings/goaccess.gpg arch=$(dpkg --print-architecture)] https://deb.goaccess.io/ $(lsb_release -cs) main" \
        | sudo tee /etc/apt/sources.list.d/goaccess.list
    $ sudo apt-get update
    $ sudo apt-get install goaccess

**注意**:
* `.deb` 包在官方仓库中也可以通过 HTTPS 获取。你可能需要安装 `apt-transport-https`。

#### Fedora ####
    # yum install goaccess

#### Arch ####
    # pacman -S goaccess

#### Gentoo ####
    # emerge net-analyzer/goaccess

#### OS X / Homebrew ####
    # brew install goaccess

#### FreeBSD ####
    # cd /usr/ports/sysutils/goaccess/ && make install clean
    # pkg install sysutils/goaccess

#### OpenBSD ####
    # cd /usr/ports/www/goaccess && make install clean
    # pkg_add goaccess

#### openSUSE ####
    # zypper ar -f obs://server:http http
    # zypper in goaccess

#### OpenIndiana ####
    # pkg install goaccess

#### pkgsrc (NetBSD, Solaris, SmartOS, ...) ####
    # pkgin install goaccess

#### Windows ####
GoAccess 可以通过 Cygwin 在 Windows 上使用。参见 Cygwin 的 <a href="https://goaccess.io/faq#installation">包</a>。或者通过 Windows 10 上的 GNU/Linux 子系统。

#### 分发包 ####
GoAccess 有最小的需求，它用 C 语言编写，只需要 ncurses。然而，以下是一些发行版中构建 GoAccess 源代码的一些可选依赖的表格。

| 发行版                 | NCurses          | GeoIP (opt)      | GeoIP2 (opt)          |  OpenSSL (opt)     |
| ---------------------- | ---------------- | ---------------- | --------------------- | -------------------|
| **Ubuntu/Debian**      | libncurses-dev   | libgeoip-dev     | libmaxminddb-dev      |  libssl-dev        |
| **RHEL/CentOS**        | ncurses-devel    | geoip-devel      | libmaxminddb-devel    |  openssl-devel     |
| **Arch**               | ncurses          | geoip            | libmaxminddb          |  openssl           |
| **Gentoo**             | sys-libs/ncurses | dev-libs/geoip   | dev-libs/libmaxminddb |  dev-libs/openssl  |
| **Slackware**          | ncurses          | GeoIP            | libmaxminddb          |  openssl           |


**注意**: 你可能需要安装构建工具，如 `gcc`, `autoconf`, `gettext`, `autopoint` 等，用于从源代码编译/构建软件。例如，`base-devel`, `build-essential`, `"Development Tools"`。

#### Docker ####
Docker 镜像已更新，能够直接从访问日志输出。如果你只想输出报告，你可以将日志从外部环境管道传输到基于 Docker 的进程：

    touch report.html
    cat access.log | docker run --rm -i -v ./report.html:/report.html -e LANG=$LANG allinurl/goaccess -a -o report.html --log-format COMBINED -

或者实时

    tail -F access.log | docker run -p 7890:7890 --rm -i -e LANG=$LANG allinurl/goaccess -a -o report.html --log-format COMBINED --real-time-html -

你可以在 [DOCKER.md](https://github.com/allinurl/goaccess/blob/master/DOCKER.md) 中阅读更多关于使用 Docker 镜像的信息。

## 贡献 ##
对 GoAccess 的任何帮助都是受欢迎的。最有帮助的方式是尝试它并提供反馈。请随时使用 GitHub 问题跟踪器和拉取请求来讨论和提交代码更改。

你可以通过直接在 GitHub 上编辑 .po 文件或使用视觉界面 [inlang.com](https://inlang.com/editor/github.com/allinurl/goaccess) 来为我们的翻译做出贡献。

[![translation badge](https://inlang.com/badge?url=github.com/allinurl/goaccess)](https://inlang.com/editor/github.com/allinurl/goaccess?ref=badge)

## 存储 ##
#### 默认哈希表 ####

内存存储提供更好的性能，代价是将数据集大小限制为可用物理内存的数量。GoAccess 使用内存哈希表。它具有非常好的内存使用情况和相当不错的性能。此存储还支持磁盘持久化。

## 命令行 / 配置选项 ##
查看可以提供给命令的 [**选项**](https://goaccess.io/man#options) 或在配置文件中指定。如果在配置文件中指定，长选项需要不使用 `--` 前缀。

## 使用 / 示例 ##
**注意**: 将数据管道传输到 GoAccess 不会提示日志/日期/时间配置对话框，你需要在配置文件或命令行中预先定义它。

### 入门 ###

输出到终端并生成交互式报告：

    # goaccess access.log

生成 HTML 报告：

    # goaccess access.log -a > report.html

生成 JSON 报告文件：

    # goaccess access.log -a -d -o report.json

生成 CSV 报告到 stdout：

    # goaccess access.log --no-csv-summary -o csv

GoAccess 还允许实时过滤和解析的极大灵活性。例如，通过监控自 goaccess 启动以来的日志来快速诊断问题：

    # tail -f access.log | goaccess -

甚至更好，使用 `tail -f` 并保持打开管道以保持实时分析，我们可以利用 `grep`、`awk`、`sed` 等匹配模式工具：

    # tail -f access.log | grep -i --line-buffered 'firefox' | goaccess --log-format=COMBINED --

或者从文件开始解析，同时保持管道打开并应用过滤器

    # tail -f -n +0 access.log | grep -i --line-buffered 'firefox' | goaccess -o report.html --real-time-html -

### 多个日志文件 ###

有几种方法可以用 GoAccess 解析多个日志。最简单的是将多个日志文件传递给命令行：

    # goaccess access.log access.log.1

即使可以解析来自管道的文件，同时读取常规文件：

    # cat access.log.2 | goaccess access.log access.log.1 -

**注意**: 单破折号附加到命令行，以让 GoAccess 知道它应该从管道中读取。

现在，如果我们想给 GoAccess 添加更多的灵活性，我们可以使用 `zcat --force` 来读取压缩和未压缩的文件。例如，如果我们想处理所有日志文件 `access.log*`，我们可以这样做：

    # zcat --force access.log* | goaccess -

**注意**: 在 Mac OS X 上，使用 `gunzip -c` 代替 `zcat`。

### 多线程支持 ###

使用 `--jobs=<count>` (或 `-j`) 启用多线程解析。例如：

    # goaccess access.log -o report.html -j 4

并使用 `--chunk-size=<256-32768>` 调整块大小，默认块大小为 1024。例如：

    # goaccess access.log -o report.html -j 4 --chunk-size=8192

### 实时 HTML 输出 ###

GoAccess 有能力在 HTML 报告中输出实时数据。你甚至可以发送 HTML 文件，因为它由一个没有外部文件依赖的单一文件组成，多么整洁！

生成实时 HTML 报告的过程与创建静态报告的过程非常相似。只需要 `--real-time-html` 就可以实现实时。

    # goaccess access.log -o /usr/share/nginx/html/your_site/report.html --real-time-html

要查看报告，你可以导航到 `http://your_site/report.html`。

默认情况下，GoAccess 将使用生成的报告的主机名。你可以选择指定客户端浏览器将连接到的 URL。有关更详细的示例，请参阅 [FAQ](https://goaccess.io/faq)。

    # goaccess access.log -o report.html --real-time-html --ws-url=goaccess.io

默认情况下，GoAccess 监听端口 7890，要使用 7890 以外的其他端口，你可以指定它（确保端口已打开）：

    # goaccess access.log -o report.html --real-time-html --port=9870

并可以将 WebSocket 服务器绑定到 0.0.0.0 以外的不同地址，你可以指定它：

    # goaccess access.log -o report.html --real-time-html --addr=127.0.0.1

**注意**: 要通过 TLS/SSL 连接输出实时数据，你需要使用 `--ssl-cert=<cert.crt>` 和 `--ssl-key=<priv.key>`。

### 过滤 ###

#### 使用日期工作 ####

另一个有用的管道将是过滤网络日志中的日期

以下将获取从 `05/Dec/2010` 开始的所有 HTTP 请求，直到文件末尾。

    # sed -n '/05\\/Dec\\/2010/,$ p' access.log | goaccess -a -

或使用相对日期，如昨天或明天：

    # sed -n '/'$(date '+%d\\/%b\\/%Y' -d '1 week ago')'/,$ p' access.log | goaccess -a -

如果我们想从日期 a 到日期 b 仅解析某个时间范围，我们可以这样做：

    # sed -n '/5\\/Nov\\/2010/,/5\\/Dec\\/2010/ p' access.log | goaccess -a -

如果我们想仅保留一定数量的数据并回收存储空间，我们可以保留最后 5 天的数据：

    # goaccess access.log --keep-last=5

#### 虚拟主机 ####

假设你的日志包含虚拟主机字段。例如：

    vhost.io:80 8.8.4.4 - - [02/Mar/2016:08:14:04 -0600] "GET /shop HTTP/1.1" 200 615 "-" "Googlebot-Image/1.0"

如果你想将虚拟主机附加到请求中，以便查看顶级 URL 属于哪个虚拟主机：

    awk '$8=$1$8' access.log | goaccess -a -

为了做同样的事情，但也使用实时过滤和解析：

    tail -f  access.log | unbuffer -p awk '$8=$1$8' | goaccess -a -

要排除一系列虚拟主机，你可以这样做：

    # grep -v "`cat exclude_vhost_list_file`" vhost_access.log | goaccess -

#### 文件、状态代码和机器人 ####

解析特定页面，例如页面浏览量，`html`, `htm`, `php` 等请求中的：

    # awk '$7~/\\.html|\\.htm|\\.php/' access.log | goaccess -

注意，`$7` 是常见和组合日志格式的请求字段（没有虚拟主机），如果你的日志包括虚拟主机，那么你可能想使用 `$8`。最好检查你瞄准的是哪个字段，例如：

    # tail -10 access.log | awk '{print $8}'

或者解析特定状态代码，例如，500（内部服务器错误）：

    # awk '$9~/500/' access.log | goaccess -

或多个状态代码，例如，所有 3xx 和 5xx：

    # tail -f -n +0 access.log | awk '$9~/3[0-9]{2}|5[0-9]{2}/' | goaccess -o out.html -

并且，获取对你的服务器的机器人（爬虫）的估计概览：

    # tail -F -n +0 access.log | grep -i --line-buffered 'bot' | goaccess -

### 小技巧 ###

同样，值得一提的是，如果我们想以较低的优先级运行 GoAccess，我们可以这样运行它：

    # nice -n 19 goaccess -f access.log -a

如果你不想在服务器上安装它，你仍然可以从你的本地机器运行它！

    # ssh -n root@server 'tail -f /var/log/apache2/access.log' | goaccess -

**注意：** SSH 需要 `-n` 以便 GoAccess 可以从标准输入读取。另外，确保使用 SSH 密钥进行身份验证，因为如果需要密码则无法工作。

#### 故障排除 ####

我们收到许多已经回答过的问题和问题。

* 日期/时间匹配问题？检查你的日志格式和你运行 GoAccess 的系统区域设置是否匹配。参见 [#1571](https://github.com/allinurl/goaccess/issues/1571#issuecomment-543186858)
* 模式匹配问题？空格经常是问题，例如 [#136](https://github.com/allinurl/goaccess/issues/136), [#1579](https://github.com/allinurl/goaccess/issues/1579)
* 其他匹配日志条目问题：参见 [>200 个关于日志/日期/时间格式的关闭问题](https://github.com/allinurl/goaccess/issues?q=is%3Aissue+is%3Aclosed+label%3A%22log%2Fdate%2Ftime+format%22)
* 日志处理问题？参见 [>111 个关于日志处理的问题](https://github.com/allinurl/goaccess/issues?q=is%3Aissue+is%3Aclosed+label%3Alog-processing)

#### 增量日志处理 ####

GoAccess 有能力通过其内部存储增量处理日志，并将数据转储到磁盘。它的工作原理如下：

1. 必须首先使用 `--persist` 持久化数据集，然后可以使用 `--restore` 加载相同的数据集。
2. 如果传入了新数据（通过管道或通过日志文件），它将将其附加到原始数据集。

##### 注意事项 #####

GoAccess 跟踪所有处理过的文件的 inode（假设文件将保留在相同的分区上），此外，它从日志中提取数据片段，以及每个文件解析的最后行和最后行解析的时间戳。例如，`inode:29627417|line:20012|ts:20171231235059`。

首先，它比较片段是否与正在解析的日志匹配，如果匹配，它假设日志没有发生剧烈变化，例如，没有被截断。如果 inode 与当前文件不匹配，它将解析所有行。如果当前文件与 inode 匹配，它将读取剩余的行，并更新解析的行数和时间戳。作为额外的预防措施，它不会解析时间戳 ≤ 存储的时间戳的日志行。

管道数据基于读取的最后一行的时间戳工作。例如，它将解析并丢弃所有传入的条目，直到找到时间戳 >= 存储的时间戳。

##### 示例 #####

    // 上个月的访问日志
    # goaccess access.log.1 --persist

然后，用它加载

    // 附加这个月的访问日志，并保留新数据
    # goaccess access.log --restore --persist

仅读取持久化数据（不解析新数据）

    # goaccess --restore

享受吧！
```

请注意，由于文档内容较长，可能存在一些翻译上的不准确之处。如果需要更精确的翻译或对特定部分有疑问，请告知。